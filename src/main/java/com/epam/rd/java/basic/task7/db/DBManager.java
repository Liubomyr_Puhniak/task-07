package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance = null;
	private Connection connection = null;

	public static synchronized DBManager getInstance() {
		if(instance == null) {
            instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			connection = DriverManager.getConnection(getURL());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		String query = "SELECT id, login FROM users";
		List<User> userList = new ArrayList<>();

		try(Statement state = connection.createStatement()) {

			ResultSet res = state.executeQuery(query);
			while(res.next()) {
				userList.add(new User(res.getInt("id"), res.getString("login")));
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		String query = "INSERT INTO users (login) VALUES (?)";
		boolean successful = false;

		try(PreparedStatement state = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

			state.setString(1, user.getLogin());
			successful = state.executeUpdate() > 0;

			ResultSet rs = state.getGeneratedKeys();
			if (rs.next()) {
				int newId = rs.getInt(1);
				user.setId(newId);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return successful;
	}

	public boolean deleteUsers(User... users) throws DBException {
		String query = "DELETE FROM users WHERE id = ?";
		boolean successful = true;

		try(PreparedStatement state = connection.prepareStatement(query)) {
			for (User user: users) {
				state.setInt(1, user.getId());
				successful = successful && (state.executeUpdate() > 0);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return successful;
	}

	public User getUser(String login) throws DBException {
		String query = "SELECT id FROM users WHERE login = ?";
		User user = null;

		try(PreparedStatement state = connection.prepareStatement(query)) {

			state.setString(1, login);
			ResultSet res = state.executeQuery();

			if(res.next()) {
				user = new User(res.getInt("id"), login);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		String query = "SELECT id FROM teams WHERE name = ?";
		Team team = null;

		try(PreparedStatement state = connection.prepareStatement(query)) {

			state.setString(1, name);
			ResultSet res = state.executeQuery();

			if(res.next()) {
				team = new Team(res.getInt("id"), name);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		String query = "SELECT id, name FROM teams";
		List<Team> teamList = new ArrayList<>();

		try(Statement state = connection.createStatement()) {

			ResultSet res = state.executeQuery(query);
			while(res.next()) {
				teamList.add(new Team(res.getInt("id"), res.getString("name")));
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		String query = "INSERT INTO teams (name) VALUES (?)";
		boolean successful = false;

		try(PreparedStatement state = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

			state.setString(1, team.getName());
			successful = state.executeUpdate() > 0;

			ResultSet rs = state.getGeneratedKeys();
			if (rs.next()) {
				int newId = rs.getInt(1);
				team.setId(newId);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return successful;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String query = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
		boolean successful = true;

		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try(PreparedStatement state = connection.prepareStatement(query)) {

			for (Team team: teams) {
				state.setInt(1, user.getId());
				state.setInt(2, team.getId());
				if (state.executeUpdate() <= 0) {
					throw new SQLException("no rows affected");
				}
			}
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			System.out.println("SQLException. Executing rollback to savepoint...");
			successful = false;
			throw new DBException("transaction failed", e);
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return successful;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		String query = "SELECT tm.id, tm.name FROM users_teams as ut" +
				" JOIN teams as tm ON tm.id = ut.team_id" +
				" WHERE ut.user_id = ?";
		List<Team> teamList = new ArrayList<>();

		try(PreparedStatement state = connection.prepareStatement(query)) {
			state.setInt(1,user.getId());
			ResultSet res = state.executeQuery();
			while(res.next()) {
				teamList.add(new Team(res.getInt("id"), res.getString("name")));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String query = "DELETE FROM teams WHERE id = ?";
		boolean successful = false;

		try(PreparedStatement state = connection.prepareStatement(query)) {
			state.setInt(1, team.getId());
			successful = state.executeUpdate() > 0;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return successful;
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = "UPDATE teams SET name = ? WHERE id = ?";
		boolean successful = true;

		try(PreparedStatement state = connection.prepareStatement(query)) {
			state.setString(1, team.getName());
			state.setInt(2, team.getId());
			successful = state.executeUpdate() > 0;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return successful;
	}

	private static String getURL() {
		Properties properties = new Properties();
		try (FileInputStream fis = new FileInputStream("app.properties")){

			properties.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties.getProperty("connection.url");
	}

}
